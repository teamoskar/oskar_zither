# Construction
## Preparation  
-   PCB manufacturing  
    <https://gitlab.opensourceecology.de/verein/projekte/cab/oskar/oskar_zither_pcb>
    -   You can order the prepared PCB at Aisler.net. <https://aisler.net/p/OPSGPOBN>
-   3D print  
    <https://gitlab.opensourceecology.de/verein/projekte/cab/oskar/oskar_zither_case>  
    -   resolution 0.2 mm
    -   infill density 30%
    -   filament PLA
-   Firmware  
    <https://gitlab.opensourceecology.de/verein/projekte/cab/oskar/oskar_firmware_arduino>
            
    Needs

    - [Arduino IDE 1.8.13](https://www.arduino.cc/en/software),
    - optional [Arduino-mk](https://github.com/mjoldfield/Arduino-Makefile),

-   Tools  
    ![tools](images/tools.jpg "tools")  
    [tools.txt](tools.txt)
-   Material  
    ![material](images/material.jpg "material")  
    [bill of materials](BOM.txt)
## Implementation
![schematic](https://gitlab.opensourceecology.de/verein/projekte/cab/oskar/oskar_zither_pcb/-/raw/assessment/images/oskar_zither_pcb.svg "schematic")
![front](https://gitlab.opensourceecology.de/verein/projekte/cab/oskar/oskar_zither_pcb/-/raw/assessment/images/oskar_zither_pcb-brd-front.svg "front")
![back](https://gitlab.opensourceecology.de/verein/projekte/cab/oskar/oskar_zither_pcb/-/raw/assessment/images/oskar_zither_pcb-brd-back.svg "back")
-   PCB solder  
    Desolder the ICSP pins (2x3 pins next to reset button on Arduino Micro) before soldering the Arduino Micro to the PCB or pinch the pins off with the side cutter as close as possible to the Arduino Micro.  
    To solder the Arduino Micro on the front side of the board, pins 2 to 9, MOSI, SCK and Ground have to be soldered.  
    For mechanical stability it is recommended to solder the 3.3V or adjacent pins. 
    Also solder the 8 keys and the 1k Ohm resistor to the front side of the board.  
    The two buttons for the thumbs are plugged into the two thumb button frames and connected to the pins labeled "R1kGND" with stranded wire.  
    Furthermore the two thumb keys are connected to the pins MOSI and SCK. Whereby the closer pins are used, left button to the left Arduino pin and right button to the right pin.  
    Connect left thumb button to SCK (shorter connection distance) and connect right thumb button to MOSI (longer connection distance). See figure solder joints.  
![solder joints](images/solderingjoints.jpg "solder joints")
-   Firmware flashing  
    -   install [Arduino IDE](https://www.arduino.cc/en/Main/Software)  
        -   install [oskar-firmware-arduino](https://gitlab.opensourceecology.de/verein/projekte/cab/oskar/oskar_firmware_arduino)  
            git clone <https://gitlab.opensourceecology.de/verein/projekte/cab/oskar/oskar_firmware_arduino.git>
    -   Using the Arduino IDE  [getting started with Arduino IDE](https://www.arduino.cc/en/Guide/Environment)

        Move the oskar_firmware_arduino projectdirectory in your Arduino scatchbook folder ("/home/user/Arduino").  
        Move the libraries from your projectdirctory/libraries to scatchbook/libraries ("/home/user/Arduino/libraries*").  
        Open the new oskar_firmware_arduino sketchbook in Arduino.  
        Select Board "Arduino Micro" then Verify/Compile.  
        Connect your Arduino Micro via USB-cable and Upload.
    -   Alternatively to the Arduino IDE user interface Arduino-mk can be used.  
        In addition to the Arduino IDE install [Arduino-mk](https://github.com/mjoldfield/Arduino-Makefile).  
        Adapt the paths ARDUINO_DIR, ARDMK_DIR and AVR_TOOLS_DIR in the Makefile to the actual paths.  
        After adjusting the Makefile compile with "make" and upload the program to the Arduino Micro with "make upload".
-   Case  
    -   Attaching the thumb keys  
        The keys snap exactly into the thumb key frames (see figure material, two thumb key frames in orange, to the left of the keys).  
        Since the keys have a square base, the orientation of the keys in the thumbnail frames is insignificant for mechanical stability.  
        The thumb key frames with the included keys are inserted into the guides in the bottom of the case with the side of the single wide frame first and the small frame last.  
        The narrow part of the thumb key frame thus faces the top or the case lid with the key panel recess.
    -   Assembly  
        Before assembling, put the keycaps on the keys.
	![inner front](images/innerfront.jpg "inner front")  
        Position the screw holes of the PCB over the screw holes of the housing base. Put on the housing cover and screw it together.  
        ![screws in case](images/screws.jpg "screws in case")  
        ![Oskar Zither front](images/oskar_zither-front.jpg "Oskar Zither front")
-   Connection  
    Connect smartphone with USB micro-B plug USB-OTG Cable and matching plug (USB micro-B, USB type-C).  
    iPhones need "Apple Camera Adapter Lightning to USB 3" with power supply via Apple Lightning connector.  
    Connect computers with USB Micro-B plug to USB Type-A plug. 
    Oskar Zither doesn't need special divers to be installed to work, it works just like any other normal keyboard on pretty much any device supporting USB keyboards.

## Usage
The 8 keys in the braille cell block correspond to the 8 dots in the braille alphabet. The 8 keys are operated with the fingers. Combined pressing of the keys results in the braille character input. The two additional keys on the shoulders can be operated with the thumbs and are used to enter spaces and additional key combinations, for example, navigation commands.

- [Erich Schmids 10 Keys Braille](https://gitlab.opensourceecology.de/verein/projekte/cab/oskar/oskar_firmware_arduino/-/blob/assessment/brailletable.md)
