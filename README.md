| Assessment according to DIN SPEC 3105 by (CAB): | [Open Source Ecology Germany e.V.](https://gitlab.opensourceecology.de/verein/projekte/cab/oseg-cab/) |
| ------ | ------ |
| Project name (version): | Oskar |
| License Hardware: | CERN-OHL-S v2 |
| License Software: | GPL-3.0-or-later |
| License Documentation: | CERN-OHL-S v2 |
| Assessment Status:| Review passed |
| Project repository:| https://gitlab.com/teamoskar/oskar_zither|




# oskar_zither
Oskar Zither is a open-source, mobile braille-keyboard.

The arrangement of 8 keys in a braille cell block and two additional keys allows Oskar Zither to be controlled without a supporting surface.

![Oskar Zither](/images/oskar_zither_whands.jpg)

# Construction
Manufacture the PCB.

3D print the case.

Solder the components.

Put the PCB in the Case and screw it together.

![Construction](/CONSTRUCTION.md)

# Bill of materials
![material](/images/material.jpg)
![bill of materials](/BOM.txt)

# Tools
![tools](/images/tools.jpg)
![tools](/tools.txt)

# Contact
[https://oskars.org](https://oskars.org)
Johannes Strelka-Petz <johannes_at_oskars.org>

# Copyright & License
    SPDX-FileCopyrightText: 2020 Johannes Strelka-Petz <johannes_at_oskars.org>
    SPDX-License-Identifier: CERN-OHL-S-2.0+
    ------------------------------------------------------------------------------
    | Copyright Johannes Strelka-Petz 2020.                                        |
    |                                                                              |
    | This source describes Open Hardware and is licensed under the CERN-OHL-S v2  |
    | or any later version.                                                        |
    |                                                                              |
    | You may redistribute and modify this source and make products using it under |
    | the terms of the CERN-OHL-S v2 or any later version                          |
    | (https://ohwr.org/cern_ohl_s_v2.txt).                                        |
    |                                                                              |
    | This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,          |
    | INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A         |
    | PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 or any later version        |
    | for applicable conditions.                                                   |
    |                                                                              |
    | Source location: https://gitlab.com/teamoskar/oskar_zither                   |
     ------------------------------------------------------------------------------
